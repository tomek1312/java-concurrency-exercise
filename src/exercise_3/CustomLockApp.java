package exercise_3;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class CustomLockApp {

    public static int counter = 0;

    public static void main(String... args) {
        firstCheck();
        System.out.println("Counter= " + counter);
//        reentrantCheck();
    }

    private static void reentrantCheck() {
        TestLock testLock = new TestLock();
        try {
            testLock.outer();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private static void firstCheck() {
        Lock lock = new Lock();
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        IntStream.range(0, 1000)
                .forEach(i -> executorService.submit(() -> {
                    try {
                        lock.lock();
                        counter++;
                        lock.unlock();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }

                }));
        try {
            executorService.shutdown();
            if (executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}

class TestLock {
    private Lock lock = new Lock();

    public void outer() throws InterruptedException {
        lock.lock();
        inner();
        lock.unlock();
    }

    public void inner() throws InterruptedException {
        lock.lock();
        System.out.println("Inner operation done!");
        lock.unlock();
    }
}

class Lock {

    public void lock() throws InterruptedException {
    }

    public void unlock() {
    }
}
