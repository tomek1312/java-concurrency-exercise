package exercise_2;


import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class PrimesCalculatorApp {

    public static void main(String[] args) {
        PrimesCalculator primesCalculator = new PrimesCalculator();
        long startTime = System.currentTimeMillis();

        IntStream.range(0, 100)
                .forEach(i -> {
                    int random = ThreadLocalRandom.current().nextInt(1, 50);
                    primesCalculator.sumFirstNPrimes(random * 1000);
                });

        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime));
    }
}


class PrimesCalculator {


    public long sumFirstNPrimes(int n) {

        long sum = 1;
        int numberOfPrimes = 0;
        int i = 0;

        while (numberOfPrimes < n) {
            i++;
            if (i % 2 != 0) {
                if (is_Prime(i)) {
                    sum += i;
                    numberOfPrimes++;
                }
            }
        }

        return sum;
    }

    public boolean is_Prime(int n) {
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }


}


